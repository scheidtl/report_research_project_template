"""
Start ipython with the command `ipython --matplotlib`

Then, run this script with:

```
run job_sim_predaprey.py
```
"""
from pathlib import Path
here = Path(__file__).absolute().parent

import matplotlib.pyplot as plt

from fluidsim.solvers.models0d.predaprey.solver import Simul

params = Simul.create_default_params()
params.A = 1.1
params.B = 0.4
params.C = 0.1
params.D = 0.4

params.time_stepping.deltat0 = 0.1
params.time_stepping.t_end = 100

params.output.periods_print.print_stdout = 0.01

sim = Simul(params)

sim.state.state_phys.set_var("X", 10)
sim.state.state_phys.set_var("Y", 10)

# sim.state.state_phys.set_var("X", sim.Xs + 0)
# sim.state.state_phys.set_var("Y", sim.Ys + 0)

# sim.output.phys_fields.plot()
sim.time_stepping.start()

sim.output.print_stdout.plot_XY()

# Note: if you want to modify the figure and/or save it, you can use
# ax = plt.gca()
# fig = ax.figure

sim.output.print_stdout.plot_XY_vs_time()

#pour save la deuxieme figure
# axx = plt.gca()
# fig2 = axx.figure

# fig_dir = here.parent / "fig"
# fig_dir.mkdir(exist_ok=True)

# fig.savefig(fig_dir / "fig_YX.png")
# fig2.savefig(fig_dir / "fig_XY_VS_Time.png")

plt.show()
